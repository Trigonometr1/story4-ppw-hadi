from django.shortcuts import render

# Create your views here.
def index(request):
    context = {
        "type" : "index"
    }
    return render(request, "story4_app/index.html", context)

def random(request):
    context = {
        "type" : "random"
    }
    return render(request, "story4_app/random.html", context)