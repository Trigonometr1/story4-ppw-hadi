from django.shortcuts import render

# Create your views here.
def index(request):
    context = {
        "type" : "story1_app"
    }
    return render(request, "story1_app/index.html", context)