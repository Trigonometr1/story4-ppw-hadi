from django.urls import path
from . import views

app_name = 'story1_app'

urlpatterns = [
    path('', views.index, name='index'),
]